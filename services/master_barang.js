$(document).ready(function() {
    get_master();
    $("#master_add").click(function () {
        getcode();
    });
})

function get_master() {
    $("#master_loading").show();
    $.ajax({
        url: "http://localhost/api-gudang/api/barang",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: {
            metadata: "true"
        },
        type: "GET",
        dataType: "json",
        success: function(response) {
            console.log(response.result);
            $tr = '';
            if (response.result.length > 0) {
                var no = 0;
                for (var i = 0; i < response.result.length; i++) {
                    no++;
                    var gambar = (response.result[i].GAMBAR == null || response.result[i].GAMBAR == "") ? url+"assets/admin/images/default.jpg" : "";
                    $tr += 
                    '<tr>'+
                        '<td>'+no+'</td>'+
                        '<td> <img class="customImages" src="'+gambar+'"></td>'+
                        '<td>'+response.result[i].PLU+'</td>'+
                        '<td>'+response.result[i].NAMA_BARANG+'</td>'+
                        '<td>'+response.result[i].MEREK_BARANG+'</td>'+
                        '<td>'+
                        '<button type="button" title="Tambahkan Gambar" class="btn blue btn-sm" data-toggle="modal" href="#gambar"><i class="fa fa-picture-o"></i></button>'+
                        '<button type="button" title="Ubah" class="btn green btn-sm"><i class="fa fa-pencil"></i></button>'+
                        '<button type="button" title="Hapus" class="btn red btn-sm" data-toggle="modal" href="#confirm_del"><i class="fa fa-trash"></i></button>'+
                        '</td>'+
                    '</tr>';         
                }
            } else {
                
            }

            $('#tabel_data tbody').html($tr);
            $("#master_loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            $("#master_loading").hide();
        }
    });
}

function get_detail_master(id) {
    $.ajax({
        url: "http://localhost/api-gudang/api/barang/"+id,
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        type: "GET",
        dataType: "json",
        success: function(response) {
            console.log(response.result);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            $("#master_loading").hide();
        }
    });
}

function openForm(id) {
    if(id != ""){ //edit data
        $("#master_form").hide();
        $("#master_data").show();
    }else{ //insert data
        $("input").val("");
        $("#master_form").hide();
        $("#master_data").show();
    }
}
function add_master() {
    var value = {
        PLU_BARANG: $("#master_plu").val(),
        NAMA_BARANG: $("#master_namabarang").val(),
        MEREK_BARANG: $("#master_merek").val()
    }

    $.ajax({
        url: "http://localhost/api-gudang/api/barang",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: value,
        type: "POST",
        dataType: "json",
        success: function(response) {
            if(response){
                get_master();
                $("input").val("");
                $("#master_form").hide();
                $("#master_data").show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function edit_master() {
    var value = {
        PLU_BARANG: $("#master_plu").val(),
        NAMA_BARANG: $("#master_namabarang").val(),
        MEREK_BARANG: $("#master_merek").val()
    }

    $.ajax({
        url: "http://localhost/api-gudang/api/barang",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: value,
        type: "POST",
        dataType: "json",
        success: function(response) {
            if(response){
                get_master();
                $("input").val("");
                $("#master_form").hide();
                $("#master_data").show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function delete_master() {
    
}

function cari_master() {
    
}

function getcode() {

    $.ajax({
        url: "http://localhost/api-gudang/api/barang/kode_barang",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        type: "GET",
        dataType: "json",
        success: function(response) {
            if(response){
                //console.log(response);
                $("#master_plu").val(response.result.plu);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}