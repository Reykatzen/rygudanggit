$(document).ready(function() {
    get_supplier();
})

function get_supplier() {
    $("#sup_loading").show();
    $.ajax({
        url: "http://localhost/api-gudang/api/supplier",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: {
            metadata: "true",
            limit: 15
        },
        type: "GET",
        dataType: "json",
        success: function(response) {
            console.log(response.result);
            $tr = '';
            if (response.result.length > 0) {
                var no = 0;
                for (var i = 0; i < response.result.length; i++) {
                    no++;
                    var tipe = 'edit';
                    $tr += 
                    '<tr>'+
                        '<td>'+no+'</td>'+
                        '<td>'+response.result[i].CODE_SUPPLIER+'</td>'+
                        '<td>'+response.result[i].NAMA_SUPPLIER+'</td>'+
                        '<td>'+response.result[i].ALAMAT_SUPPLIER+'</td>'+
                        '<td>'+response.result[i].TELPON_SUPPLIER+'</td>'+
                        '<td>'+response.result[i].EMAIL_SUPPLIER+'</td>'+
                        '<td>'+
                        '<button type="button" title="Ubah" class="btn green btn-sm" onclick="openForm(\"'+tipe+'\",'+response.result[i].ID+')"><i class="fa fa-pencil"></i></button>'+
                        '<button type="button" title="Hapus" class="btn red btn-sm" onclick="openDelete('+response.result[i].ID+')"><i class="fa fa-trash"></i></button>'+
                        '</td>'+
                    '</tr>';         
                }
            } else {
                
            }

            $('#tabel_data tbody').html($tr);
            $("#sup_loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            $("#sup_loading").hide();
        }
    });
}

function openForm(type,id = "") {
    $("#tipe").val(type);
    if(type == "edit" ){ //edit data
        $("#sup_form").show();
        $("#sup_data").hide();
        $("#sup_id").val(id);
        get_detail_supplier(id,"edit");
        console.log('edit');
    }
    if(type == "add"){ //insert data
        $("#formInput input").val("");
        $("#sup_id").empty();
        $("#sup_form").show();
        $("#sup_data").hide();
        console.log('insert');
    }
}

function openDelete(id) {
    $("#popup_del").click();
    $("#id_delete").val(id);
    get_detail_supplier(id,"delete");
}

function get_detail_supplier(id,type) {
    $.ajax({
        url: "http://localhost/api-gudang/api/supplier/detail/"+id,
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        type: "GET",
        dataType: "json",
        success: function(response) {
            console.log(response.result);
            if (type == "edit") {
                
                $("#sup_code").val(response.result.CODE_SUPPLIER);
                $("#sup_nama").val(response.result.NAMA_SUPPLIER);
                $("#sup_alamat").val(response.result.ALAMAT_SUPPLIER);
                $("#sup_telpon").val(response.result.TELPON_SUPPLIER);
                $("#sup_email").val(response.result.EMAIL_SUPPLIER);
            }
            if (type == "delete") {
                $("#pesan_del").html("Yakin ingin hapus supplier "+response.result.NAMA_SUPPLIER+" ini ?");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            $("#sup_loading").hide();
        }
    });
}


function add_supplier() {
    var value = {
        CODE_SUPPLIER: $("#sup_code").val(),
        NAMA_SUPPLIER: $("#sup_nama").val(),
        ALAMAT_SUPPLIER: $("#sup_alamat").val(),
        TELPON_SUPPLIER: $("#sup_telpon").val(),
        EMAIL_SUPPLIER: $("#sup_email").val()
    }

    var tipe = $("#tipe").val();

    if (tipe == 'edit') {
        $.ajax({
            url: "http://localhost/api-gudang/api/supplier/"+idsup,
            beforeSend: function (xhr){
                xhr.setRequestHeader("authorization", localStorage.getItem("token"));
            },
            data: value,
            type: "PUT",
            dataType: "json",
            success: function(response) {
                if(response){
                    get_supplier();
                    $("#formInput input").val("");
                    $("#tipe").val("");
                    $("#sup_form").hide();
                    $("#sup_data").show();
                    notif("success", "Data berhasil diperbarui");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                notif("error", xhr.status);
            }
        });
    }

    if(tipe == 'add'){
        $.ajax({
            url: "http://localhost/api-gudang/api/supplier",
            beforeSend: function (xhr){
                xhr.setRequestHeader("authorization", localStorage.getItem("token"));
            },
            data: value,
            type: "POST",
            dataType: "json",
            success: function(response) {
                if(response){
                    get_supplier();
                    $("#formInput input").val("");
                    $("#tipe").val("");
                    $("#sup_form").hide();
                    $("#sup_data").show();
                    notif("success", "Data berhasil simpan");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr);
                console.log(thrownError);
                notif("error", xhr.responseJSON.message);
            }
        });
    }

}

function delete_supplier() {
    var idsup = $("#id_delete").val();
    $.ajax({
        url: "http://localhost/api-gudang/api/supplier/"+idsup,
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        type: "DELETE",
        dataType: "json",
        success: function(response) {
            if(response){
                get_supplier();
                $("#id_delete").val("");
                $("#batal_del").click();
                notif("success", "Data berhasil dihapus");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            notif("error", xhr.status);
        }
    });
}

function getcode() {
    var namasup = $("#sup_nama").val();
    if (namasup != "") {
        var value = {
            NAMA_SUPPLIER: $("#sup_nama").val()
        }
        
        $.ajax({
            url: "http://localhost/api-gudang/api/supplier/kode_supplier",
            beforeSend: function (xhr){
                xhr.setRequestHeader("authorization", localStorage.getItem("token"));
            },
            data: value,
            type: "GET",
            dataType: "json",
            success: function(response) {
                if(response){
                    $("#sup_code").val(response.result.kode);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    }
    if(namasup === ""){
        console.log("kosong");
        $("#sup_code").val("");
    }

}