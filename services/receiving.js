$(document).ready(function() {
    get_receiving();
})

function get_receiving() {
    $("#rec_loading").show();
    $.ajax({
        url: "http://localhost/api-gudang/api/receiving",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: {
            metadata: "true"
        },
        type: "GET",
        dataType: "json",
        success: function(response) {
            console.log(response.result);
            $tr = '';
            if (response.result.length > 0) {
                var no = 0;
                for (var i = 0; i < response.result.length; i++) {
                    no++;
                    $tr += 
                    '<tr>'+
                        '<td>'+no+'</td>'+
                        '<td>'+response.result[i].CODE_RECEIVING+'</td>'+
                        '<td>'+response.result[i].NAMA_SUPPLIER+'</td>'+
                        '<td>'+response.result[i].TANGGAL_DATANG+'</td>'+
                        '<td>'+response.result[i].TANGGAL_TERIMA+'</td>'+
                        '<td>'+response.result[i].TOTAL_QTY+'</td>'+
                        '<td>'+response.result[i].TOTAL_HARGA+'</td>'+
                        '<td>'+
                        '<button type="button" title="Ubah" class="btn green btn-sm"><i class="fa fa-pencil"></i></button>'+
                        '<button type="button" title="Hapus" class="btn red btn-sm" data-toggle="modal" href="#confirm_del"><i class="fa fa-trash"></i></button>'+
                        '</td>'+
                    '</tr>';         
                }
            } else {
                
            }

            $('#tabel_data tbody').html($tr);
            $("#rec_loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            $("#rec_loading").hide();
        }
    });
}

function get_detail_receiving(id) {
    $.ajax({
        url: "http://localhost/api-gudang/api/receiving/"+id,
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        type: "GET",
        dataType: "json",
        success: function(response) {
            console.log(response.result);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            $("#rec_loading").hide();
        }
    });
}

function openForm(id) {
    if(id != ""){ //edit data
        $("#rec_form").hide();
        $("#rec_data").show();
    }else{ //insert data
        $("input").val("");
        $("#rec_form").hide();
        $("#rec_data").show();
    }
}
function add_receiving() {
    var value = {
        CODE_RECEIVING: $("#rec_code").val(),
        ID_SUPPLIER: $("#rec_idsup").val(),
        NAMA_SUPPLIER: $("#rec_namasup").val(),
        TANGGAL_DATANG: $("#rec_datang").val(),
        TANGGAL_TERIMA: $("#rec_terima").val(),
        TOTAL_QTY: $("#rec_totalqty").val(),
        TOTAL_HARGA: $("#rec_totalharga").val()
    }

    $.ajax({
        url: "http://localhost/api-gudang/api/receiving",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: value,
        type: "POST",
        dataType: "json",
        success: function(response) {
            if(response){
                get_receiving();
                $("input").val("");
                $("#rec_form").hide();
                $("#rec_data").show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function edit_receiving() {
    var value = {
        CODE_RECEIVING: $("#rec_code").val(),
        ID_SUPPLIER: $("#rec_idsup").val(),
        NAMA_SUPPLIER: $("#rec_namasup").val(),
        TANGGAL_DATANG: $("#rec_datang").val(),
        TANGGAL_TERIMA: $("#rec_terima").val(),
        TOTAL_QTY: $("#rec_totalqty").val(),
        TOTAL_HARGA: $("#rec_totalharga").val()
    }

    $.ajax({
        url: "http://localhost/api-gudang/api/receiving",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: value,
        type: "POST",
        dataType: "json",
        success: function(response) {
            if(response){
                get_receiving();
                $("input").val("");
                $("#rec_form").hide();
                $("#rec_data").show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function delete_receiving() {
    
}

function getcode() {
    var value = {
        CODE_RECEIVING: $("#rec_code").val()
    }

    $.ajax({
        url: "http://localhost/api-gudang/api/receiving/kode_supplier",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: value,
        type: "GET",
        dataType: "json",
        success: function(response) {
            if(response){
                $("#rec_code").val(response.result.kode);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}