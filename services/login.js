function proses_login() {
    var value = {
        USERNAME: $("#username").val(),
        PASSWORD: $("#password").val()
    }

    $.ajax({
        url: "http://localhost/api-gudang/api/auth/login",
        data: value,
        type: "POST",
        dataType: "json",
        success: function(response) {
            if(response.token){
                localStorage.setItem("token", response.token);
                for (var i = 0; i < response.result.length; i++) {
                    localStorage.setItem("username", response.result[i].USERNAME);
                }
                // var http = new XMLHttpRequest();
                // http.setRequestHeader("Authorization", response.token);
                window.location.href = url+"home";
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function proses_logout() {
    var value = {
        USERNAME: localStorage.getItem("username")
    }

    $.ajax({
        url: "http://localhost/api-gudang/api/auth/logout",
        beforeSend: function (xhr){
            xhr.setRequestHeader("authorization", localStorage.getItem("token"));
        },
        data: value,
        type: "POST",
        dataType: "json",
        success: function(response) {
            localStorage.clear();
            window.location.href = url+"login";
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}

function cek_afterlogout() {
    var token = localStorage.getItem("token");
    if(token==="" || token===null){
        window.location.href = url+"login";
    }
}