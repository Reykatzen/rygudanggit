<!-- DATA -->
<div class="row" id="sup_data">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase"> DAFTAR SUPPLIER</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only blue" title="Tambah Supplier" href="javascript:;" onclick="openForm('add')">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>

            <div id="sup_loading">
                <img src="<?=base_url();?>assets/admin/layout/img/loading.gif" alt="loading"/>
            </div>
            
            <div class="portlet-body">
                <!-- Searching -->
                <div class="form-group has-info">
                    <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                        </span>
                        <input type="text" class="form-control" id="sup_cari" placeholder="Cari Supplier">
                    </div>
                </div>

                <!-- Table -->
                <div class="table-scrollable">
                    <table class="table table-hover" id="tabel_data">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> Code </th>
                        <th> Nama </th>
                        <th> Alamat </th>
                        <th> Telpon </th>
                        <th> Email </th>
                        <th> ACT. </th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

<!-- FORM -->
<div class="row" id="sup_form">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase"> FORM INPUT SUPPLIER</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only red" title="Close" id="sup_formcancel" href="javascript:;">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <input type="hidden" id="tipe">
                <form role="form" id="formInput">
                    <div class="form-body">
                        <!-- ID -->
                        <input type="hidden" id="sup_id">

                        <!-- NAMA -->
                        <div class="form-group form-md-line-input has-info col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control" id="sup_nama" onchange="getcode()" placeholder="Nama Supplier">
                                <label class="control-label">
                                    NAMA
                                    <span class="required"> * </span>
                                </label>
                            </div>
                        </div>

                        <!-- CODE -->
                        <div class="form-group form-md-line-input has-info col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-barcode"></i>
                                </span>
                                <input type="text" readonly class="form-control" id="sup_code" placeholder="Code Supplier">
                                <label class="control-label">
                                    CODE
                                    <span class="required"> * </span>
                                </label>
                            </div>
                        </div>

                        <!-- TELPON -->
                        <div class="form-group form-md-line-input has-info col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-phone"></i>
                                </span>
                                <input type="text" class="form-control" id="sup_telpon" placeholder="Telpon Supplier">
                                <label class="control-label">
                                    TELPON
                                    <span class="required"> * </span>
                                </label>
                            </div>
                        </div>
                        
                        <!-- EMAIL -->
                        <div class="form-group form-md-line-input has-info col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-envelope"></i>
                                </span>
                                <input type="email" class="form-control" id="sup_email" placeholder="Email Supplier">
                                <label class="control-label">
                                    EMAIL
                                    <span class="required"> * </span>
                                </label>
                            </div>
                        </div>
                        
                        <!-- ALAMAT -->
                        <div class="form-group form-md-line-input has-info col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-map-marker"></i>
                                </span>
                                <input type="text" class="form-control" id="sup_alamat" placeholder="Alamat Supplier">
                                <label class="control-label">
                                    ALAMAT
                                    <span class="required"> * </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-actions noborder">
                        <button type="button" class="btn blue" onClick="add_supplier()">
                            <i class="fa fa-save"></i>    
                            Simpan Data
                        </button>
                        <button type="resset" class="btn red">
                            <i class="fa fa-times"></i>
                            Batalkan
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

<!-- MODAL CONFIRM DELETE -->
<button type="button" id="popup_del" data-toggle="modal" href="#confirm_del" style="display:none;">dialog</button>
<div class="modal fade bs-modal-sm" id="confirm_del" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Konfirmasi Hapus</h4>
            </div>
            <div class="modal-body">
                <p id="pesan_del"></p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="id_delete">
                <button type="button" class="btn default" data-dismiss="modal" id="batal_del">Batal</button>
                <button type="button" class="btn red" onclick="delete_supplier()">Ya</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- SCRIPT -->
<script src="<?=base_url();?>js_gudang/jquery-1.11.1.min.js"></script>
<script src="<?=base_url();?>services/supplier.js"></script>
<script>
$(document).ready(function() {
    $("#sup_form").hide();
    $("#sup_loading").hide();
    
    $("#sup_formcancel").click(function () {
        $("#formInput input").val("");
        $("#tipe").val("");
        $("#sup_id").val("");
        $("#sup_form").hide();
        $("#sup_data").show();
    });
})
</script>