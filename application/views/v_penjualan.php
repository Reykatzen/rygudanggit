<!-- DATA -->
<div class="row" id="sell_data">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase"> DAFTAR PENJUALAN</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only blue" title="Form Penjualan" id="sell_add" href="javascript:;">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>

            <div id="sell_loading">
                <img src="<?=base_url();?>assets/admin/layout/img/loading.gif" alt="loading"/>
            </div>

            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover" id="tabel_data">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> Code Receive</th>
                        <th> Nama Suplier </th>
                        <th> Tgl. Datang </th>
                        <th> Tgl. Terima </th>
                        <th> Total Qty. </th>
                        <th> Total Harga </th>
                        <th> ACT. </th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

<!-- FORM -->
<div class="row" id="sell_form">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase"> FORM PENJUALAN</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only red" title="Close" id="sell_formcancel" href="javascript:;">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                        <!-- CODE RECEIVE -->
                        <div class="form-group has-info col-md-12">
                            <label class="control-label">
                                CODE RECEIVE
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-barcode"></i>
                                </span>
                                <input type="text" readonly class="form-control" id="sell_code" placeholder="Code Receive">
                            </div>
                        </div>

                        <!-- ID SUPPLIER -->
                        <div class="form-group has-info col-md-6">
                            <label class="control-label">
                                ID SUPPLIER
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-circle"></i>
                                </span>
                                <select class="form-control select2me" id="sell_idsup" name="options2">
                                    <option value="">Select...</option>
                                    <option value="Option 1">Option 1 - Describe A</option>
                                    <option value="Option 2">Option 2 - Describe B</option>
                                    <option value="Option 3">Option 3 - Describe C</option>
                                    <option value="Option 4">Option 4 - Describe D</option>
                                    <option value="Option 4">Option 5 - Describe E</option>
                                </select>
                            </div>
                        </div>

                        <!-- NAMA SUPPLIER -->
                        <div class="form-group has-info col-md-6">
                            <label class="control-label">
                                NAMA SUPPLIER
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control" id="sell_namasup" placeholder="Nama Supplier">
                            </div>
                        </div>
                        
                        <!-- TANGGAL DATANG -->
                        <div class="form-group has-info col-md-6">
                            <label class="control-label">
                                TANGGAL DATANG
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                                </span>
                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                    <input type="text" class="form-control" id="sell_datang" readonly name="datepicker">
                                    <span class="input-group-btn">
                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <!-- TANGGAL TERIMA -->
                        <div class="form-group has-info col-md-6">
                            <label class="control-label">
                                TANGGAL TERIMA
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                                </span>
                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                    <input type="text" class="form-control" id="sell_terima" readonly name="datepicker">
                                    <span class="input-group-btn">
                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                        <!-- TOTAL QTY -->
                        <div class="form-group has-info col-md-6">
                            <label class="control-label">
                                TOTAL QTY.
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-th-list"></i>
                                </span>
                                <input type="text" class="form-control" id="sell_totalqty" placeholder="Total QTY.">
                            </div>
                        </div>

                        <!-- TOTAL HARGA -->
                        <div class="form-group has-info col-md-6">
                            <label class="control-label">
                                TOTAL HARGA
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-jpy"></i>
                                </span>
                                <input type="text" class="form-control" id="sell_totalharga" placeholder="Total Harga">
                            </div>
                        </div>
                    </div>

                    <!-- CEKDATE -->
                        <!-- <div class="form-group">
                            <label class="control-label col-md-2">Datepicker</label>
                            <div class="col-md-10">
                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                                    <input type="text" class="form-control" readonly name="datepicker">
                                    <span class="input-group-btn">
                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                                //input-group//
                                <span class="help-block">
                                select a date </span>
                            </div>
                        </div> -->
                        <!-- CEKDATE -->
                    
                    <div class="form-actions noborder">
                    <button type="button" class="btn blue" onClick="add_receive()">
                            <i class="fa fa-save"></i>    
                            Simpan Data
                        </button>
                        <button type="resset" class="btn red">
                            <i class="fa fa-times"></i>
                            Batalkan
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

<!-- MODAL CONFIRM DELETE -->
<div class="modal fade bs-modal-sm" id="confirm_del" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Konfirmasi Hapus</h4>
            </div>
            <div class="modal-body">
                    Yakin ingin menghapus
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn red">Ya</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- SCRIPT -->
<script src="<?=base_url();?>js_gudang/jquery-1.11.1.min.js"></script>
<script src="<?=base_url();?>services/receiving.js"></script>
<script>
$(document).ready(function() {
    $("#sell_form").hide();
    $("#sell_loading").hide();
    $("#sell_add").click(function () {
        $("#sell_form").show();
        $("#sell_data").hide();
    });
    $("#sell_formcancel").click(function () {
        $("input").val("");
        $("#sell_form").hide();
        $("#sell_data").show();
    });
})
</script>