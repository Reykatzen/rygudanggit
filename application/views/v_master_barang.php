<!-- DATA -->
<div class="row" id="master_data">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase"> DAFTAR MASTER BARANG</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only blue" title="Tambah Master Barang" id="master_add" href="javascript:;">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>

            <div id="master_loading">
                <img src="<?=base_url();?>assets/admin/layout/img/loading.gif" alt="loading"/>
            </div>

            <div class="portlet-body">
                <!-- Searching -->
                <div class="form-group has-info">
                    <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-search"></i>
                        </span>
                        <input type="text" class="form-control" id="master_cari" placeholder="Cari Barang">
                    </div>
                </div>

                <!-- Table -->
                <div class="table-scrollable">
                    <table class="table table-hover" id="tabel_data">
                    <thead>
                    <tr>
                        <th> # </th>
                        <th> Gambar </th>
                        <th> PLU </th>
                        <th> Nama Barang </th>
                        <th> Merek Barang </th>
                        <th> ACT. </th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

<!-- FORM -->
<div class="row" id="master_form">
    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase"> FORM INPUT MASTER BARANG</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only red" title="Close" id="master_formcancel" href="javascript:;">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                        <!-- PLU -->
                        <div class="form-group has-info">
                            <label class="control-label">
                                PLU
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-barcode"></i>
                                </span>
                                <input type="text" class="form-control" id="master_plu" placeholder="PLU" readonly>
                            </div>
                        </div>

                        <!-- NAMA BARANG -->
                        <div class="form-group has-info">
                            <label class="control-label">
                                NAMA BARANG
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-cube"></i>
                                </span>
                                <input type="text" class="form-control" id="master_namabarang" placeholder="Nama Barang">
                            </div>
                        </div>

                        <!-- MEREK BARANG -->
                        <div class="form-group has-info">
                            <label class="control-label">
                                MEREK BARANG
                                <span class="required"> * </span>
                            </label>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-tag"></i>
                                </span>
                                <input type="text" class="form-control" id="master_merek" placeholder="Merek Barang">
                            </div>
                        </div>                        
                        
                        <!-- TEST DROPZONE -->
                        <p>
                            <span class="label label-danger">
                            NOTE: </span>
                            &nbsp; This plugins works only on Latest Chrome, Firefox, Safari, Opera & Internet Explorer 10.
                        </p>
                        <form action="<?=base_url();?>assets/global/plugins/dropzone/upload.php" class="dropzone" id="my-dropzone">
                        </form>
                        <!-- TEST DROPZONE -->
                        
                    </div>
                    
                    <div class="form-actions noborder">
                        <button type="button" class="btn blue" onClick="add_master()">
                            <i class="fa fa-save"></i>    
                            Simpan Data
                        </button>
                        <button type="resset" class="btn red">
                            <i class="fa fa-times"></i>
                            Batalkan
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

<!-- MODAL CONFIRM DELETE -->
<div class="modal fade bs-modal-sm" id="confirm_del" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Konfirmasi Hapus</h4>
            </div>
            <div class="modal-body">
                    Yakin ingin menghapus
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn red">Ya</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- SCRIPT -->
<script src="<?=base_url();?>js_gudang/jquery-1.11.1.min.js"></script>
<script src="<?=base_url();?>services/master_barang.js"></script>
<script>
$(document).ready(function() {
    $("#master_form").hide();
    $("#master_loading").hide();
    $("#master_add").click(function () {
        $("#master_form").show();
        $("#master_data").hide();
    });
    $("#master_formcancel").click(function () {
        $("input").val("");
        $("#master_form").hide();
        $("#master_data").show();
    });
})
</script>