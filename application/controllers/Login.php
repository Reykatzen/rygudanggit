
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
    	parent::__construct();

	}

	public function index()
	{
		$data = array(
			'pagetitle'	=> 'RY Gudang | Login'
		);
		$this->load->view('v_login', $data);
	}

}