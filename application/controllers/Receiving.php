<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Receiving extends CI_Controller {

	function __construct() {
    	parent::__construct();

	}

	public function index()
	{
		$data = array(
			'page' 			=> 'v_receiving',
			'pagetitle'		=> 'RY Gudang | Receiving Barang',
			'page_header'	=> 'Receiving Barang'
		);
		$this->load->view('v_main', $data);
	}

}