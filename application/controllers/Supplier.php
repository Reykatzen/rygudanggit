<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	function __construct() {
    	parent::__construct();

	}

	public function index()
	{
		$data = array(
			'page' 			=> 'v_supplier',
			'pagetitle'		=> 'RY Gudang | Supplier',
			'page_header'	=> 'Supplier'
		);
		$this->load->view('v_main', $data);
	}

}