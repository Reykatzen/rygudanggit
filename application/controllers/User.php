<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
    	parent::__construct();

	}

	public function index()
	{
		$data = array(
			'page' 			=> 'v_user',
			'pagetitle'		=> 'RY Gudang | User',
			'page_header'	=> 'User'
		);
		$this->load->view('v_main', $data);
	}

}