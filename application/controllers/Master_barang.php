<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_barang extends CI_Controller {

	function __construct() {
    	parent::__construct();

	}

	public function index()
	{
		$data = array(
			'page' 			=> 'v_master_barang',
			'pagetitle'		=> 'RY Gudang | Master Barang',
			'page_header'	=> 'Master Barang'
		);
		$this->load->view('v_main', $data);
	}

}