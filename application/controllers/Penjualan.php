<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	function __construct() {
    	parent::__construct();

	}

	public function index()
	{
		$data = array(
			'page' 			=> 'v_penjualan',
			'pagetitle'		=> 'RY Gudang | Penjualan',
			'page_header'	=> 'Penjualan'
		);
		$this->load->view('v_main', $data);
	}

}