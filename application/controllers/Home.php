<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
    	parent::__construct();

	}

	public function index()
	{
		$data = array(
			'page' 			=> 'v_home',
			'pagetitle'		=> 'RY Gudang | Home',
			'page_header'	=> 'Dashboard'
		);
		$this->load->view('v_main', $data);
	}

}